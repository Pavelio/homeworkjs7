function arrayListDOM(arr) {
    if (arr.length > 0) {
        arr.map(function(element) {
            let ul = document.createElement('ul');
            ul.innerHTML +=`<li>${element}</li>`;
            document.body.appendChild(ul);
        });
    }
    else {
        alert("Array = 0");
    }

    function startTimer(duration, display) {
        let timer = duration, seconds;
        setInterval(function () {
            seconds = parseInt(timer % 60, 10);
            display.textContent = seconds;
            if (--timer < 0) {
                timer = duration;
            }
        }, 1000);
    }
    window.onload = function () {
        let second = 9,
            display = document.getElementById('timer');
        startTimer(second, display);
    }
    setTimeout(function () {
        document.body.remove();
    }, 10000);
}
let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
arrayListDOM(array);